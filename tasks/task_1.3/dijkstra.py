from collections import defaultdict


class Dijkstra:
    @staticmethod
    def find_path(nodes, neighbors, start, end):
        prev = {start: None}
        mark = {}
        for node in range(1, nodes + 1):
            mark[node] = float('inf')
        mark[start] = 0

        while True:
            temp = list(set(range(1, nodes + 1)).difference(prev.keys()))
            if len(temp) == 0:
                break
            temp.sort(key=lambda x: mark[x])
            for (neighbor, weight) in neighbors[temp[0]]:
                mark[neighbor] = min(mark[neighbor], mark[temp[0]] + weight)
                if mark[temp[0]] + weight < mark[neighbor]:
                    prev[neighbor] = temp[0]
        current = end
        result = [current]
        while current in prev.keys():
            result.append(prev[current])
            current = prev[current]
        return result

    @staticmethod
    def find_paths(node_count, weights, start):
        nodes = {n for n in range(1, node_count + 1)}
        distance = {start: 0}
        prev = {start: 0}
        nodes -= {start}
        for node in nodes:
            distance[node] = weights[start - 1][node - 1]
            prev[node] = start
        for k in range(2, len(nodes)):
            tmp = list(nodes)
            tmp.sort(key=lambda x: distance[x])
            w = tmp[0]
            nodes -= {w}
            for v in nodes:
                if distance[v] > distance[w] + weights[w - 1][v - 1]:
                    prev[v] = w
                distance[v] = min(
                    distance[v], distance[w] + weights[w - 1][v - 1]
                )
        return distance, prev


def main():
    with open('in.txt') as f:
        lines = list(map(str.split, f.read().strip().replace('   ', ' ').split('\n')))
    nodes = int(lines[0][0])
    start = int(lines[-2][0])
    end = int(lines[-1][0])
    lines = lines[1:-2]
    weights = [list(map(int, l)) for l in lines]

    for start in range(1, nodes + 1):
        distance, prev = Dijkstra.find_paths(nodes, weights, start)
        print(start)
        for e in range(1, nodes + 1):
            print(distance[e], end=' ')
        print()
        for e in range(1, nodes + 1):
            print(prev[e], end=' ')
        print('\n')

    current = end
    path = []
    while current != 0:
        path.append(current)
        if current in prev.keys():
            current = prev[current]
        else:
            path = None
            break
    path.reverse()
    with open('out.txt', 'w') as f:
        if path is None:
            f.write('N')
        else:
            f.write('Y\n')
            for e in path:
                f.write(str(e) + ' ')
            f.write('\n')
            m = 1
            for (i, j) in zip(range(len(path) - 1), range(1, len(path))):
                    m *= weights[path[i] - 1][path[j] - 1]
            f.write(str(m))


if __name__ == '__main__':
    main()
