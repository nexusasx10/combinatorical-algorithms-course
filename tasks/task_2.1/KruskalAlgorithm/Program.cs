﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace KruskalAlgorithm
{
    class Program
    {
        static void Combine(int[] Name, int[] Next, int[] Size, int v, int w, int p, int q)
        {
            Name[w] = p;
            var u = Next[w];
            while(Name[u] != p)
            {
                Name[u] = p;
                u = Next[u];
            }
            Size[p] = Size[p] + Size[q];
            var w1 = Next[w];
            var v1 = Next[v];
            Next[w] = v1;
            Next[v] = w1;
        }

        static void Main(string[] args)
        {
            string data = File.ReadAllText("./in.txt");
            int[] array = Regex.Split(data, "\\s+")
                .Select(s => { if (s != "") return int.Parse(s); else return 0; })
                .ToArray();
            List<int> indexes = new List<int>();
            List<int> V = new List<int>();
            List<Tuple<int, int, int>> E = new List<Tuple<int, int, int>>();
            for (int i = 1; i < array.Length; i++)
            {
                indexes.Add(array[i]);
                if ( i != 0 && array[i] == array[0])
                {
                    break;
                }
            }
            for (int i = 0; i < indexes.Count - 1; i++)
            {
                int index = indexes[i];
                int nextIndex = indexes[i + 1];
                V.Add(i);
                for (var j = index; j < nextIndex; j += 2)
                {
                    if (!E.Contains(new Tuple<int, int, int>(array[j] - 1, i, array[j + 1])))
                    {
                        E.Add(new Tuple<int, int, int>(i, array[j] - 1, array[j + 1]));
                    }
                }
                if (i == indexes.Count - 1)
                {
                    break;
                }
            }

            int n = indexes.Count - 1;
            var Name = new int[n];
            var Size = new int[n];
            var Next = new int[n];
            Queue<Tuple<int, int, int>> queue = new Queue<Tuple<int, int, int>>(E.OrderBy(x => x.Item3));


            foreach (var v in V)
            {
                Name[v] = v;
                Size[v] = 1;
                Next[v] = v;
            }
            var T = new HashSet<Tuple<int, int, int>>();
            while (T.Count < n - 1)
            {
                var vw = queue.Dequeue();
                var v = vw.Item1;
                var w = vw.Item2;
                var p = Name[v];
                var q = Name[w];
                if (p != q)
                {
                    if (Size[q] < Size[p])
                    {
                        Combine(Name, Next, Size, v, w, p, q);
                    }
                    else
                    {
                        Combine(Name, Next, Size, w, v, q, p);
                    }
                    T.Add(vw);
                }
            }
            StringBuilder builder = new StringBuilder();
            int weight = 0;
            for (int i = 0; i < n; i++)
            {
                var node = i + 1;
                var neighbors = T.Where(x => x.Item2 + 1 == node);
                weight += neighbors.Select(x => x.Item3).Sum();
                var line = string.Join(" ", T.
                    Select(x => x.Item2 + 1 == node ? x.Item1 + 1 : x.Item1 + 1 == node ? x.Item2 + 1 : 0)
                    .Where(x => x != 0)
                    .OrderBy(x => x)
                );
                builder.AppendLine(line == "" ? "0" : line + " 0");
            }
            builder.Append(weight);
            File.WriteAllText("out.txt", builder.ToString());
        }
    }
}
