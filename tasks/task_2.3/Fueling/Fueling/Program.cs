﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Fueling
{
    class Program
    {
        static void Main(string[] args)
        {
            string data = File.ReadAllText("./in.txt");
            double[] array = Regex.Split(data, "\\s+")
                .Where(s => s != "")
                .Select(s => double.Parse(s))
                .ToArray();
            Dictionary<int, Tuple<int, double>> fuelings = new Dictionary<int, Tuple<int, double>>();
            double pathLength = array[0];
            int capacity = (int)array[1];
            int consumption = (int)array[2];
            double startPay = array[3];
            int n = (int)array[4];
            for (int i = 0; i < n; i++)
            {
                fuelings[i] = new Tuple<int, double>((int)array[5 + i * 2], array[6 + i * 2]);
            }
            for (int i = 0; i < )
            Dictionary<int, List<Tuple<int, double>>> neighbors = new Dictionary<int, List<Tuple<int, double>>>();


        }

        public static List<int> FindPath(Dictionary<int, List<Tuple<int, double>>> neighbors)
        {
            Dictionary<int, int> prev = new Dictionary<int, int> { { 0, -1 } };
            Dictionary<int, double> mark = new Dictionary<int, double>();
            for (var node = 1; node < nodes + 1; node++) {
                mark[node] = float.PositiveInfinity;
            }
            mark[0] = 0;
            List<int> temp;
            while (true) {
                temp = new List<int>(new HashSet<int>(Enumerable.Range(1, nodes + 1)).Distinct(prev.Keys));
                if (temp.Count == 0) {
                    break;
                }
                temp.OrderBy(x => mark[x]);
                foreach(var neighbor in neighbors[temp[0]])
                {
                    mark[neighbor.Item1] = min(mark[neighbor.Item1], mark[temp[0]] + neighbor.Item2);
                    if (mark[temp[0]] + neighbor.Item2 < mark[neighbor.Item1])
                    {
                        prev[neighbor.Item1] = temp[0];
                    }
                }
                
            }
            var current = end;
            List<int> result = new List<int>() { current };
            while (prev.Keys.Contains(current)) {
                result.Add(prev[current]);
                current = prev[current];
            }
            return result;
        }
        

        
        
    }
}
