﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace FullMatching
{
    class Program
    {
        static void Main(string[] args)
        {
            string data = File.ReadAllText("./in.txt");
            int[] array = Regex.Split(data, "\\s+")
                .Where(s => s != "")
                .Select(s => int.Parse(s))
                .ToArray();
            List<int> indexes = new List<int>();
            HashSet<int> X = new HashSet<int>(Enumerable.Range(0, array[0]));
            HashSet<int> Y = new HashSet<int>(Enumerable.Range(0, array[1]));
            HashSet<Tuple<int, int>> E = new HashSet<Tuple<int, int>>();
            for (int i = 3; i < array.Length; i++)
            {
                indexes.Add(array[i] + 2);
                if (array[i] == array[2])
                {
                    break;
                }
            }
            for (int i = 0; i < indexes.Count - 1; i++)
            {
                int index = indexes[i];
                if (index == 2)
                {
                    continue;
                }
                int nextIndex;
                int k = i;
                while (true)
                {
                    nextIndex = indexes[k + 1];
                    if (nextIndex == 2)
                    {
                        k++;
                    }
                    else
                    {
                        break;
                    }
                }
                for (int j = index; j < nextIndex; j++)
                {
                    E.Add(new Tuple<int, int>(i, array[j] - 1));
                }
                i = k;
            }

            int[] xPair = Build(X, Y, E);
            if (xPair.Contains(-1))
            {
                File.WriteAllLines("./out.txt", new string[] { "N", (Array.IndexOf(xPair, -1) + 1).ToString() });
            }
            else
            {
                string r = String.Join(" ", xPair.Select(x => x + 1));
                File.WriteAllLines("./out.txt", new string[] { "Y", r });
            }
        }

        static int[] Build(HashSet<int> X, HashSet<int> Y, HashSet<Tuple<int, int>> E)
        {
            bool[] visited = new bool[X.Count];
            int[] XP = new int[X.Count];
            for (int i = 0; i < X.Count; i++)
            {
                XP[i] = -1;
            }
            int[] YP = new int[Y.Count];
            for (int i = 0; i < Y.Count; i++)
            {
                YP[i] = -1;
            }
            while (true)
            {
                int white = Array.IndexOf(XP, -1);
                if (white == -1)
                {
                    break;
                }
                if (!Dfs(white, XP, YP, E, visited))
                {
                    break;
                }
            }
            return XP;
        }

        static bool Dfs(int begin, int[] XP, int[] YP, HashSet<Tuple<int, int>> E, bool[] visited)
        {
            Stack<int> stack = new Stack<int>();
            int[] fromX = new int[E.Count];
            int[] fromY = new int[E.Count];
            fromX[begin] = -2;
            stack.Push(begin);
            while (stack.Count > 0)
            {
                begin = stack.Pop();
                if (visited[begin])
                {
                    continue;
                }
                visited[begin] = true;
                int[] ends = E.Where(x => x.Item1 == begin).Select(x => x.Item2).ToArray();
                foreach (int end in ends)
                {
                    fromY[end] = begin;
                    if (YP[end] == -1)
                    {
                        int current = end;
                        while (true)
                        {
                            ExtendM(XP, YP, new Tuple<int, int>(fromY[current], current));
                            current = fromY[current];
                            if (fromX[current] == -2)
                            {
                                return true;
                            }
                            current = fromX[current];
                        }
                    }
                    else
                    {
                        begin = YP[end];
                        fromX[begin] = end;
                        stack.Push(begin);
                    }
                }
            }
            return false;
        }

        static void ExtendM(int[] XP, int[] YP, Tuple<int, int> newEdge)
        {
            int left = YP[newEdge.Item2];
            int right = XP[newEdge.Item1];
            if (left != -1)
            {
                XP[left] = -1;
            }
            if (right != -1)
            {
                YP[right] = -1;
            }
            XP[newEdge.Item1] = newEdge.Item2;
            YP[newEdge.Item2] = newEdge.Item1;
        }
    }
}
