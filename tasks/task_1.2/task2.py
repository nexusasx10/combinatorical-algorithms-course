

class CycleFinder:
    def __init__(self):
        self.graph = None
        self.neighbors = []

    def read_input(self, path):
        try:
            with open(path) as file:
                input_data = file.read()
        except OSError:
            print('in file not found')
        if input_data.endswith('0'):
            input_data += '\n'
        input_data = input_data.replace(' 0\n', '\n')
        input_data = input_data.split('\n')
        if input_data[-1] == '':
            input_data = input_data[:-1]
        nodes = int(input_data[0])
        for i in range(nodes):
            a = input_data[i + 1].split(' ')
            self.neighbors.append(list(map(int, a)))

    def find(self, start):
        to_visit_stack = [start]
        visited = set()
        came_from = {start: None}
        while len(to_visit_stack) > 0:
            item = to_visit_stack.pop()
            visited.add(item)
            neighbors = list(self.neighbors[item - 1])
            neighbors.reverse()
            for neighbor in neighbors:
                if neighbor == came_from[item]:
                    continue
                came_from[neighbor] = item
                if neighbor in visited:
                    result = []
                    start = neighbor
                    while neighbor in came_from.keys() and came_from[neighbor] != start:
                        result.append(neighbor)
                        neighbor = came_from[neighbor]
                    result.append(neighbor)
                    result.sort()
                    return result
                to_visit_stack.append(neighbor)


if __name__ == '__main__':
    cycle_finder = CycleFinder()
    cycle_finder.read_input('in.txt')
    cycle = cycle_finder.find(1)
    try:
        with open('out.txt', 'w') as file:
            if cycle is None:
                file.write('A')
            else:
                file.write('N\n')
                for node in cycle:
                    file.write(str(node) + ' ')
    except OSError:
        print('Out file not found')
