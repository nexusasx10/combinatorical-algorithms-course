from collections import deque as Queue


class PathFinder:
    def __init__(self):
        self.maze = None
        self.order = (-1, 0), (1, 0), (0, -1), (0, 1)

    def read_input(self, path):
        try:
            with open(path) as file:
                input_data = file.read()
        except OSError:
            print('in file not found')
        input_data = input_data.replace('\n', ' ')
        input_data = input_data.split(' ')
        height = int(input_data[0])
        width = int(input_data[1])
        i = width * height + 2
        data = input_data[2:i]
        start = int(input_data[i]), int(input_data[i + 1])
        end = int(input_data[i + 2]), int(input_data[i + 3])
        self.maze = Maze(width, height, data, start, end)

    def find(self):
        to_visit_queue = Queue()
        to_visit_queue.append(self.maze.start)
        came_from = {}
        visited = set()
        find = False
        while len(to_visit_queue) > 0:
            item = to_visit_queue.popleft()
            visited.add(item)
            for (o_x, o_y) in self.order:
                neighbor = (item[0] + o_x, item[1] + o_y)
                if self.maze[neighbor] and neighbor not in visited:
                    to_visit_queue.append(neighbor)
                    came_from[neighbor] = item
            if item == self.maze.end:
                find = True
        if not find:
            return None
        result = []
        current = self.maze.end
        while True:
            result.append(current)
            if current == self.maze.start:
                return result
            current = came_from[current]


class Maze:
    def __init__(self, width, height, data, start, end):
        self.width = width
        self.height = height
        data_iter = iter(data)
        self.start = start
        self.end = end
        self._maze = [[not bool(int(next(data_iter))) for x in range(width)] for y in range(height)]

    def __getitem__(self, item):
        return self._maze[item[0] - 1][item[1] - 1]


if __name__ == '__main__':
    path_finder = PathFinder()
    path_finder.read_input('in.txt')
    path = path_finder.find()
    try:
        with open('out.txt', 'w') as file:
            if path is None:
                file.write('N')
            else:
                file.write('Y\n')
                path.reverse()
                for item in path:
                    file.write('{} {}\n'.format(item[0], item[1]))

    except OSError:
        print('Out file not found')
